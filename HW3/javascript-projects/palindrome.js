var words = [
    "token",
    "Glenelg",
    "hippopotamus",
    "racecar",
    "a man a plan a canal panama",
    "Javascript"
         ];

for(var j = 0;j<words.length;j++){
    if(palindrome(words[j])){
        console.log("Palindrome!")
    }else{
        console.log("Not a Palindrome....")
    }
}


function palindrome(word){
    word = word.toLowerCase().split(" ").join("").trim();
    var boundary = Math.ceil(word.length/2.0);
    for(var i = 0; i<boundary;i++){
        if(word[i] != word[word.length-i-1]){
            return false;
        }
    }
    return true;
}