/**
 * Created by Bernardo on 2/4/2016.
 */
var aux={s:0,t:0};
for(var i = 0; i<10;i++){
    aux = generate_Normal();
    console.log(aux.s);
    console.log(aux.t);
}

function generate_Normal(){
    v = Math.random();
    u = Math.random();
    while(u === 0){
        u = Math.random();
    }
    var result = {
        s: (Math.sqrt(-2*Math.log(u))* Math.cos(2*Math.PI*v)),
        t: (Math.sqrt(-2*Math.log(u))* Math.sin(2*Math.PI*v))
    };
    return result;
}