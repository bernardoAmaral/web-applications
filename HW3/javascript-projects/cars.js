/**
 * Created by Bernardo on 2/5/2016.
 */

function Car(make,model,year,owner){
    this.make = make;
    this.model = model;
    this.year = year;
    this.owner = owner;
}

function Person(name,cars){
    this.name = name;
    this.cars = cars;
}

gary = new Person("Gary", []);
susan = new Person("Susan", []);
bob = new Person("Bob", []);
aline = new Person("Aline", [])
guilhermo = new Person("Guilhermo",[])

car1 = new Car("Cevy", "Volt", 2013,gary);
car2 = new Car("Dodge", "Ram", 2009, susan);
car3 = new Car("Tesla","X P90D",2016, bob);
car4 = new Car("Toyota","Tacoma",2004,gary)
car5 = new Car("Ford","Mustang",2014, bob)
car6 = new Car("Subaru","Forester",2007, aline)
car7 = new Car("Audi","TT",2012, bob)
car8 = new Car("Toyota","Camry",1995,susan)
car9 = new Car("Ford","Taurus",1998, aline)
car10 = new Car("BMW","M4",2011,aline)
car11 = new Car("Acura","TL",2009, guilhermo)

gary.cars = [car1,car4]
susan.cars = [car2,car8]
bob.cars = [car3,car5,car7]
aline.cars = [car6,car9,car10]
guilhermo.cars = [car11]

persons = [gary,susan,bob,aline,guilhermo]

for(var i = 0; i< persons.length;i++){
    for(var j = 0; j<persons[i].cars.length;j++){
        console.log("%s owns a %d %s model %s",persons[i].name, persons[i].cars[j].year,persons[i].cars[j].make,persons[i].cars[j].model)
    }
}