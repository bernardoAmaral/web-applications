var array_to_sort = [7,2,5,6,1,1,9,3,1];

console.log("Unsorted array");
console.log(array_to_sort);

// Write the sorting code here //

var sorted = false;
while(!sorted){
    sorted = true;
    for(var i = 0;i<array_to_sort.length-1;i++){
        if(array_to_sort[i]>array_to_sort[i+1]){
            aux = array_to_sort[i];
            array_to_sort.splice(i,1,array_to_sort[i+1]);
            array_to_sort.splice(i+1,1,aux);
            sorted = false
        }else if(array_to_sort[i]===array_to_sort[i+1]){
            array_to_sort.splice(i,1)
        }
    }
}

// End sorting code //

console.log("Sorted array");
console.log(array_to_sort);