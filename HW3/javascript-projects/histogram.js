/**
 * Created by Bernardo on 2/5/2016.
 */

var samples = [];
for(var i = 0;i<5000;i++){
    var aux = generate_Normal();
    samples.push(aux.s);
    samples.push(aux.t);
}


    var hist = [];
    for(var j = -3.0; j <=3.0;j= j+0.1){
        hist.push({bin:j,counter:0});
    }
    for(var sample = 0;sample<samples.length;sample++){
        var col = 0
            while(col<hist.length-1  && samples[sample] > hist[col].bin) {
                col++;
            }
            hist[col].counter++;
    }
    var max = 0;
    for(var k = 0;k<hist.length;k++){
        max = Math.max(max,hist[k].counter);
    }
    for(var l = 0; l<hist.length; l++){
        hist[l].counter = hist[l].counter/max;
    }
    for(var m = 1;m>0;m = m-0.05){
        var drawings = [];
        for(var n =0;n<hist.length;n++){
            if(hist[n].counter >= m) {
                drawings.push("*")
            }else
            {
                drawings.push(" ")
            }

        }
        console.log('%s %s %s %s %s %s %s %s %s %s %s %s %' +
            's %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %' +
            's %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' +
            ' %s %s %s %s %s %s %s %s %s %s',drawings[0],drawings[1],drawings[2],drawings[3],drawings[4],drawings[5],drawings[6],drawings[7],drawings[8],drawings[9],drawings[10],drawings[11],
            drawings[12],drawings[13],drawings[14],drawings[15],drawings[16],drawings[17],drawings[18],drawings[19],drawings[20],drawings[21],drawings[22],drawings[23],drawings[24],drawings[25],
            drawings[26],drawings[27],drawings[28],drawings[29],drawings[30],drawings[31],drawings[32],drawings[33],drawings[34],drawings[35],drawings[36],drawings[37],drawings[38],drawings[39],
            drawings[40],drawings[41],drawings[42],drawings[43],drawings[44],drawings[45],drawings[46],drawings[47],drawings[48],drawings[49],drawings[50],drawings[51],drawings[52],drawings[53],
            drawings[54],drawings[55],drawings[56],drawings[57],drawings[58],drawings[59])
    }


function generate_Normal(){
    v = Math.random();
    u = Math.random();
    while(u === 0){
        u = Math.random();
    }
    var result = {
        s: (Math.sqrt(-2*Math.log(u))* Math.cos(2*Math.PI*v)),
        t: (Math.sqrt(-2*Math.log(u))* Math.sin(2*Math.PI*v))
    };
    return result;
}