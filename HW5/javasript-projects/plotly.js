/**
 * Created by jesus on 2/12/16.
 */
"use strict";
/* replace yourUsername, yourKey and yourToken with those items
   after you sign up in plotly and obtain a stream token */

var APP = (function() {
    var plotly = require('plotly')('BernardoCostaAmaral', 'kq5y0w7b10');
    var data = [{x:[], y:[], stream:{token:'bfr1o8lfub', maxpoints:366}}];
    var graphOptions = {fileopt : "overwrite", filename : "example"};

    plotly.plot(data,graphOptions,function() {
        var stream = plotly.stream('bfr1o8lfub', function (err,res) {
            console.log(err,res);
            clearInterval(loop);
        });
        var i=0;
        var loop = setInterval(function () {
            var arg = i/180*Math.PI;
            var s = Math.sin(arg);
            var streamObject = JSON.stringify({ x : arg, y : s });
            stream.write(streamObject+'\n');
            i++;
        }, 10);
    });
}());