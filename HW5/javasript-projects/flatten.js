"use strict";

// Return an array with the contents of arr flattened to one dimension
function flatten(arr) {
    // Input: an Array containing both primitives and arrays
    // Output: an Array containing all of the primitive elements of arr
    // ex. if input === [1, [2, 3, [4,5]], [[[[6]]]]]
    //     then output === [1, 2, 3, 4, 5, 6]
    return arr.reduce(function (a, b) {
        if(Array.isArray(a)){
            a = flatten(a)
        }
        if (Array.isArray(b)) {
            b = flatten(b)
        }
        return [].concat(a).concat(b)
    } )


}

console.log(flatten([[[1, [0, 1.1]], 2, 3], [[6, 7], [4, 5]]]));
