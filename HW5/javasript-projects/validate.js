"use strict";

var test_passwords = [
    "Hxou7p&&3",
    "password",
    ";larJ7",
    "DROWSSAP",
    "K33pI7S@f3",
    ":Belieber1"
];

var success = [function () {
    console.log(this + " is a valid password")
}]
var wrong_size = function (small_size,large_size) {
    console.log("Password must be between "+ small_size + " and " + large_size +" characters!")
}
var no_lower_case = function () {
    console.log("Password must contain a lowercase character!")
}
var no_upper_case = function () {
    console.log("Password must contain a uppercase character!")
}
var no_number = function () {
    console.log("Password must contain a number!")
}
var no_special = function () {
    console.log("Password must contain a special character!")
}


var error = [wrong_size,no_lower_case,no_upper_case,no_number,no_special]
var regexArray = [RegExp("[a-z]+"),RegExp("[A-Z]+"),RegExp("[0-9]+"), RegExp("[!$%&#;?@~]+")]
var i;


String.prototype.validate = function(small_size, large_size, regex_paterns,success_callback, error_calback){
    var error = false
    if(this.length < small_size || this.length > large_size){
        error_calback[0].call(this, small_size,large_size);
        error = true
    }
    for(var i = 0;i< regex_paterns.length;i++){
        if(!this.match(regex_paterns[i])){
            error_calback[i+1].call(this);
            error = true
        }
    }
    if(error === false){
        success_callback[0].call(this)
    }
}
for (i=0; i < test_passwords.length; i++) {
    console.log(test_passwords[i])
    test_passwords[i].validate(8,20,regexArray,success,error);

}
