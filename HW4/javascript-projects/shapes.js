"use strict";

/* Implement Shape, Triangle, and Rectangle here */

/* Implement Shape, Triangle, and Rectangle here */


var Shape = function(){
    var args = arguments
    if (arguments.length===1) {
        args = arguments[0];
    }
    return{

        pointSet: args,
        perimeter : function () {
            var aux = 0, i, a, b, xd, yd;
            for (i = 0; i < this.pointSet.length; i++) {
                a = this.pointSet[i % this.pointSet.length];
                b = this.pointSet[(i + 1) % this.pointSet.length];
                aux += Math.sqrt((a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]));
            }
            return aux;
        }
    }
}

var Triangle = function () {

    var that = Shape(arguments)
    that.area = function () {
        var a = Math.sqrt(Math.pow(that.pointSet[0][0] - that.pointSet[1][0],2)+ Math.pow(that.pointSet[0][1] - that.pointSet[1][1],2));
        var b = Math.sqrt(Math.pow(that.pointSet[1][0] - that.pointSet[2][0],2)+ Math.pow(that.pointSet[1][1] - that.pointSet[2][1],2));
        var c = Math.sqrt(Math.pow(that.pointSet[2][0] - that.pointSet[0][0],2)+ Math.pow(that.pointSet[2][1] - that.pointSet[0][1],2));
        var semi_per = that.perimeter()/2;
        return Math.sqrt((semi_per)*(semi_per-a)*(semi_per-b)*(semi_per-c))
    }
    return that;
};

var Rectangle = function () {
    var that = Shape(arguments)
    that.area = function () {
        var a = Math.sqrt(Math.pow(this.pointSet[0][0] - this.pointSet[1][0],2)+ Math.pow(this.pointSet[0][1] - this.pointSet[1][1],2));
        var b = Math.sqrt(Math.pow(this.pointSet[1][0] - this.pointSet[2][0],2)+ Math.pow(this.pointSet[1][1] - this.pointSet[2][1],2));
        return a*b;
    }
    return that;
};


var shape = new Shape([0,0], [0,7], [3,4], [1,2]);

var tri = new Triangle([0,0], [2,1], [2,0]);

var rect  = new Rectangle([0,0], [1,0], [1,1], [0,1]);

var perimeter, area;

if (shape.hasOwnProperty("perimeter")) {
    perimeter = shape.perimeter();
}
else {
    perimeter = "No perimeter attribute";
}

if (shape.hasOwnProperty("area")) {
    area = shape.area();
}
else {
    area = "No area attribute";
}

console.log("shape", perimeter, area);


if (tri.hasOwnProperty("perimeter")) {
    perimeter = tri.perimeter();
}
else {
    perimeter = "No perimeter attribute";
}

if (tri.hasOwnProperty("area")) {
    area = tri.area();
}
else {
    area = "No area attribute";
}

console.log("tri", perimeter, area);


if (rect.hasOwnProperty("perimeter")) {
    perimeter = rect.perimeter();
}
else {
    perimeter = "No perimeter attribute";
}

if (rect.hasOwnProperty("area")) {
    area = rect.area();
}
else {
    area = "No area attribute";
}

console.log("rect", perimeter, area);