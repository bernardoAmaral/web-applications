'use strict';

/* Implement your TreeNode and BinaryTree classes here */

/* Implement your TreeNode and BinaryTree classes here */

var TreeNode = function (data) {
    this.data = data;
    this.count = 0;
    this.left = null;
    this.right = null;
    this.compare = function (number) {
        if(number === this.data){
            return this;
        }else if(number < this.data){
            return this.left;
        }else{
            return this.right;
        }
    }
    this.print = function(){
        if(this.left === null && this.right === null){
            console.log("<,"+ this.data +",>")
        }else if(this.left != null && this.right === null){
            console.log("<" + this.left.print() + ", "+ this.data + ", >")
        }else if(this.left === null && this.right != null){
            console.log("<, "+ this.data + ", "+ this.right.print() + ">")
        }else{
            console.log("<" + this.left.print() + ", "+ this.data + ", "+ this.right.print() + ">")
        }

    }

}

var BinaryTree = function () {
    this.root = null;
    this.insert = function (number) {
        var pointer = this.root;
        while(true){
            if(pointer === null){
                pointer = new TreeNode(number);
                pointer.count++;
                return
            }else{
                var next = pointer.compare(number);
                if(next === pointer){
                    pointer.count++;
                    return;
                }else{
                    pointer = next
                }
            }
        }
    }
    this.find = function (number) {
        var pointer = this.root;
        while(true){
            if(pointer === null){
                return false
            }else{
                var next = pointer.compare(number)
                if(next === pointer){
                    return true;
                }else{
                    pointer = next
                }
            }
        }
    }
    this.remove = function (number) {
        var pointer = this.root;
        var parent = null;
        while(true){
            if(pointer === null){
                return
            }else{
                var next = pointer.compare(number)
                if(next === pointer){
                    if(pointer.count > 0){
                        pointer.count--;
                        return
                    }else{
                        if(pointer.left === null && pointer.right === null){
                            if(parent.right === pointer){
                                parent.right = null
                            }else{
                                parent.left = null
                            }
                            pointer = null
                        }else if(pointer.left === null && !pointer.right === null){
                            if(parent.right === pointer){
                                parent.right = pointer.right
                            }else{
                                parent.left = pointer.right
                            }
                            pointer = pointer.right
                        }else if(pointer.left != null && pointer.right === null){
                            if(parent.right === pointer){
                                parent.right = pointer.left
                            }else{
                                parent.left = pointer.left
                            }
                            pointer = pointer.left
                        }else{
                            var left_most = pointer.right
                            var left_most_parent = pointer;
                            while(left_most.left != null){
                                left_most_parent = left_most
                                left_most = left_most.left
                            }
                            left_most_parent.left = left_most.right
                            left_most.right = pointer.right
                            left_most.left = pointer.left
                            if(parent.right === pointer){
                                parent.right = left_most
                            }else{
                                parent.left = left_most
                            }
                        }
                    }
                }else{
                    parent = pointer
                    pointer = next
                }
            }
        }
    }
    this.print = function () {
        console.log(this)
        if(this.root != null){
            this.root.print();
        }else{

            console.log("<,,>")
        }
    };

}


var tree = new BinaryTree();

tree.print()
tree.root = new TreeNode(1)
tree.insert(2)
tree.print()
var values = [32, 1, 53, 4, 6, 16, 2, 5, 5, 7, 3, 11, 23]

/*
var i;

for (i = 0 ; i < values.length; i++) {
    tree.insert(values[i]);
}

tree.print();

tree.remove(4);

tree.print();

tree.remove(5);

tree.print();

tree.insert(11);

tree.print();

if (tree.find(53)) {
    console.log("Found 53");
}

else {
    console.log("Error: did not find 53");
}

if (!tree.find(111)) {
    console.log("111 not in the tree");
}
else {
    console.log("Error: found 111");
}
*/