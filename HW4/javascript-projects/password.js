"use strict";

/* Implement String.validate() here */


String.prototype.validate = function(small_size, large_size, valid_regex_paterns,invalid_regex_paterns){
    if(this.length < small_size || this.length > large_size){
        return false;
    }
    for(var i = 0;i< valid_regex_paterns.length;i++){
        if(!this.match(valid_regex_paterns[i])){
            return false;
        }
    }
    for(var j = 0;j< invalid_regex_paterns.length;j++){
        if(this.match(invalid_regex_paterns[j])){
            return false;
        }
    }
    return true;
}


/* Implement String.validate() here */

var test_passwords = [
    "Hxou7p&&3",
    "password",
    ";larJ7",
    "DROWSSAP",
    "K33pI7S@f3",
    ":Belieber1"
];
for(var i= 0; i< test_passwords.length;i++){
    if(test_passwords[i].validate(8,20,[RegExp("[a-z]+"),RegExp("[A-Z]+"),RegExp("[0-9]+"), RegExp("[!$%&#;?@~]+")],[RegExp("[^[a-z]]|[^[A-Z]]|[^[0-9]]|[^[!$%&#;?@~]]")])){
        console.log("Password " + test_passwords[i] + " is valid!")
    }else{
        console.log("Password " + test_passwords[i] + " is invalid!")
    }
}

