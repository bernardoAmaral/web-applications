/**
 * Created by Bernardo on 2/10/2016.
 */


var Firebase = require('firebase');
var bitcoinRef = new Firebase("https://publicdata-cryptocurrency.firebaseio.com/bitcoin");

function showBid(snapshot){
    console.log("Bid", +snapshot.val());
}
function showAsk(snapshot){
    console.log("Ask" + snapshot.val());
}


function showLast(snapshot){
    console.log("Last" + snapshot.val());
}
function queryData(){
    bitcoinRef.child("bid").once("value", showBid);
    bitcoinRef.child("ask").once("value",showAsk);
    bitcoinRef.child("last").once("value",showLast);

}

console.log("Bitcoin");
setInterval (queryData,1000);