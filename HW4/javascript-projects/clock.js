/**
 * Created by Bernardo on 2/10/2016.
 */

var Clock = function(steps,alarms_array) {
    this.time = new Date().getTime();
    this.timestep =  steps;
    this.alarms = alarms_array.sort();
    this.alarm =  function(){
        console.log("It's time!!! Wake up!!!");
    }
    this.addAlarm = function(extra_alarm){
        extra_alarm += (this.timestep - (extra_alarm-this.time)%this.timestep);
        this.alarms.push(extra_alarm);
        this.alarms.sort();
    }
    this.showTime = function () {
        console.log(this.time)
    }
    this.tick = function(){
        this.time += this.timestep;

        if(this.time === this.alarms[0]){
            this.alarm();
            this.alarms.splice(0,1)
        }
        if(this.alarms.length === 0){
            clearInterval(this.clockid)
        }
    }
    this.clockid = setInterval(function(clock){clock.tick()},this.timestep, this);

}

time = new Date().getTime();
var a = new Clock(1000,[time + 3000, time + 5000,time + 7000,time + 9000,time + 11000,time + 13000,time + 15000])
