/**
 * Created by Bernardo on 2/26/2016.
 */
"use strict"

var ref = new Firebase("https://bamaral.firebaseio.com/restaurants");
// Attach an asynchronous callback to read the data at our posts reference
ref.on("value", function(snapshot) {
    for( var keys in snapshot.val()){
        var $li = $("<li>"+snapshot.val()[keys].name+"</li>")
        $("ul").append($li)
        $li.data(snapshot.val()[keys])
        //$("#"+snapshot.val()[keys].name).data(snapshot.val()[keys])
        console.log($li.data())
    }


}, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
});

var show_details_handler = function (event) {
    var data = $(event.target).data()
    console.log(data)
    var $section = $("<section></section>")
    $section.append($("<h3>"+data.name+"</h3>"))
    $section.append($("<p>"+data.street+"</p>"))
    $section.append($("<p>"+data.city + ", "+ data.state+" "+ data.zip+"</p>"))
    $section.append($("<a href = "+data.site+" >"+data.site+"</a>"))
    var $list = $section.append($("<ol></ol>"))
    for(var i = 0; i<data.menu.length;i++){
        $list.append($("<li class = 'item'>"+data.menu[i].name+"</li>"))
        $list.append($("<p class = 'item'>"+data.menu[i].price+"</p>"))
        $list.append($("<p class = 'item'>"+data.menu[i].description+"</p>"))
    }

    $section.insertAfter(event.target)
    $("ul").off("click", "li", show_details_handler)
    $("ul").on("dblclick", "section",hide_details_handler)
}

var hide_details_handler = function (event) {
    $("section").html("")
    $("ul").off("dblclick", "section",hide_details_handler)
    $("ul").on("click", "li", show_details_handler)
}

$("ul").on("click", "li", show_details_handler)
