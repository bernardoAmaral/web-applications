// This is supposed to work on simple.html
// in the homework 6 starter code

// Write code to remove an item on double click
// add show_details_handler to the ul
// print the item's text() and html() to the console
"use strict";
$('ul').on('dblclick', 'li', function (e) {
    console.log(e.target.textContent)
    console.log(e.timeStamp)
    e.target.remove()
})